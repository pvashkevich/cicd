@RestResource(urlMapping='/webhook/*')
global with sharing class ViberCallbackHandler {
    global static final String EVENTMESSAGE = 'message';
    global static final String EVENTSUBSCRIBED = 'subscribed';
    global static final String EVENTUNSUBSCRIBED = 'unsubscribed';

    @HttpGet
    global static String getText() {
        System.debug('@HttpGet is called');
        return 'HTTP GET called';
    }

    @HttpPost
    global static String putText() {
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String requestString = body.toString();

        // finding out the event type
        Map<String, Object> mapRequestString = (Map<String, Object>) JSON.deserializeUntyped(requestString);
        System.debug('@HttpPost is called, Body =' + mapRequestString);
        String event = (String) mapRequestString.get('event');
        System.debug('event = ' + event);
        if  (event == EVENTMESSAGE) {
            // income message
            Map<String, Object> sender = (Map<String, Object>) mapRequestString.get('sender');
            String userId = (String) sender.get('id');
            String userName = (String) sender.get('name');
            Map<String, Object> message = (Map<String, Object>) mapRequestString.get('message');
            ViberCallbackhelper.eventMessage(userId, userName, message);
        }
        if  (event == EVENTSUBSCRIBED) {
            // viber user was subscribed
            Map<String, Object> sender = (Map<String, Object>) mapRequestString.get('user');
            String userId = (String) sender.get('id');
            String userName = (String) sender.get('name');
            ViberCallbackhelper.eventSubscribed(userId, userName);
        }
        if  (event == EVENTUNSUBSCRIBED) {
            // viber user was ussubscribed
            String userId = (String) mapRequestString.get('user_id');
            ViberCallbackhelper.eventUnsubscribed(userId);
        }

        return 'OK';
    }
}